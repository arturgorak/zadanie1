#include "klasy.h"
#include <iostream>

using namespace std;

Sort::Sort(int *array, int size) {
    this->array = array;
    this->size = size;
}

void Sort ::print() {
    for(int i = 0; i < size; i++){
        cout << array[i] << " ";
    }
    cout << endl;
    this->test();
}

void Sort ::sort() {
    for(int j = 1; j < size; j++){
        int element = array[j];
        int i = j - 1;
        while(i >= 0 && array[i] > element){
            array[i + 1] = array[i];
            i = i - 1;
        }
        array[i + 1] = element;
    }
}

void Sort ::test() {
    for(int i = 0; i < size - 1; i++){
        if(array[i] > array[i + 1]){
            cout << "the array is not sorted" << endl;
            return;
        }
    }
    cout << "the array is sorted" << endl;
}

void Sort::sort2(){
    this->merge_sort(0, size - 1);
}

void Sort::merge(int start, int mid, int end){


    int *temp = new int[end-start];
    int i = start, j = mid+1, k = 0;

    while (i <= mid && j <= end){

        if (array[j] < array[i]){
            temp[k] = array[j];
            j++;
        }
        else{
            temp[k] = array[i];
            i++;
        }
        k++;
    }

    if (i <= mid){
        while (i <= mid){
            temp[k] = array[i];
            i++;
            k++;
        }
    }
    else{
        while (j <= end){
            temp[k] = array[j];
            j++;
            k++;
        }
    }

    for (i = 0; i <= end-start; i++) {
        array[start + i] = temp[i];
    }
    delete[] temp;

}

void Sort::merge_sort(int start, int end){
    int mid;

    if (start != end){
        mid = (start + end)/2;
        merge_sort(start, mid);
        merge_sort(mid+1, end);
        merge(start, mid, end);
    }
}

void Sort::heapSort() {
    Heap x(this->array,this->size);

    x.buildHeap();

    for (int i=size-1; i>0; i--)
    {
        int temp = array[0];
        array[0] = array[i];
        array[i] = temp;

        x.Heapify(i, 0);
    }
}

Heap::Heap(int *array, int size) {
    this->array = array;
    this->size = size;
    this->fullSize = size;
}

void Heap::Heapify (int heap_size, int i)
{
    int largest = i;
    int l = LeftChild(i);
    int r = RightChild(i);

    if (l < heap_size && array[l] > array[largest])
        largest = l;

    if (r < heap_size && array[r] > array[largest])
        largest = r;

    if (largest != i)
    {
        swap(array[i], array[largest]);

        Heapify(heap_size, largest);
    }
}

void Heap::parent(int index) {
    if(index != 0){
        cout << "Parent: " << array[(index-1) >> 1] << endl;
    }
    else{
        cout << "No parent" << endl;
    }

}

void Heap::buildHeap() {
    for (int i = size / 2 - 1; i >= 0; i--)
        Heapify(size, i);
}

int Heap::ExtraMax() {
    int res = array[0];
    for(int i = 0; i < size - 1; i++){
        array[i] = array[i + 1];
    }
    size--;
    Heapify(size, 0);

    return res;
}

int Heap::returnSize() {
    return size;
}

void Heap::insert(int x) {
    if(size < fullSize){
        array[size] = x;
        size++;
        Heapify(size,0);
    }
    else{
        fullSize = size<<1;
        int newArray[fullSize];
        for(int i = 0; i < size; i++){
            newArray[i] = array[i];
        }
        array[size] = x;
        size++;
        *array = *newArray;
        buildHeap();
    }
}

int Heap::RightChild(int x) {
    return (x<<1) + 2;
}

int Heap::LeftChild(int x) {
    return (x<<1) + 1;
}

BST::BST(int value) {
    this->value = value;
    this->left = NULL;
    this->right = NULL;
}

BST& BST::insert(int value) {
    BST* currentNode = this;
    while (true){
        if(value < currentNode -> value){
            if(currentNode -> left == NULL){
                BST* newNode = new BST(value);
                currentNode->left = newNode;
                break;
            }
            else{
                currentNode = currentNode->left;
            }
        }
        else{
            if(currentNode->right == NULL){
                BST* newNode = new BST(value);
                currentNode->right = newNode;
                break;
            }else{
                currentNode = currentNode->right;
            }

        }
    }
    return *this;
}

bool BST::search(int value) {
    BST* currentNode = this;
    while(currentNode != NULL){
        if(value < currentNode -> value){
            currentNode = currentNode -> left;
        }
        else if(value > currentNode -> value){
            currentNode = currentNode -> right;
        }
        else{
            return true;
        }
    }
}

BST& BST::remove(int value, BST *parentNode) {
    BST* currentNode = this;
    while(currentNode != NULL) {
        if(value < currentNode->value){
            parentNode = currentNode;
            currentNode = currentNode->left;
        }else if(value > currentNode->value){
            parentNode = currentNode;
            currentNode = currentNode->right;
        }else{
            if(currentNode->left != NULL && currentNode->right != NULL){
                currentNode->value = currentNode->right->getMinValue();
                currentNode->right->remove(currentNode->value, currentNode);
            }else if(parentNode == NULL){
                if(currentNode->left != NULL){
                    currentNode->value = currentNode->left->value;
                    currentNode->right=currentNode->left->right;
                    currentNode->left = currentNode->left->left;
                }else if(currentNode->right != NULL){
                    currentNode->value = currentNode->right->value;
                    currentNode->left = currentNode->right->left;
                    currentNode->right=currentNode->right->right;
                }else{
                    currentNode->value = 0;
                }
            }else if(parentNode->left == currentNode){
                if(currentNode->left != NULL){
                    parentNode->left = currentNode->left;
                }else if(currentNode->right != NULL){
                    parentNode->right = currentNode->right;
                }
            }else if(parentNode->right == currentNode){
                if(currentNode->left != NULL){
                    parentNode->right = currentNode->left;
                }else {
                    parentNode->right = currentNode->right;
                }
            }
            break;
        }
    }
    return  *this;
}

int BST::getMinValue() {
     BST* currentNode = this;
     while(currentNode->left != NULL){
         currentNode = currentNode->left;
     }
     return currentNode->value;
}

void RedBlackTree::initializeNULLNode(NodePtr node, NodePtr parent) {
    node->data = 0;
    node->parent = parent;
    node->left = nullptr;
    node->right = nullptr;
    node->color = 0;
}

void RedBlackTree::preOrderHelper(NodePtr node) {
    if (node != TNULL) {
        cout << node->data << " ";
        preOrderHelper(node->left);
        preOrderHelper(node->right);
    }
}

void RedBlackTree::inOrderHelper(NodePtr node) {
    if (node != TNULL) {
        inOrderHelper(node->left);
        cout << node->data << " ";
        inOrderHelper(node->right);
    }
}

void RedBlackTree::postOrderHelper(NodePtr node) {
    if (node != TNULL) {
        postOrderHelper(node->left);
        postOrderHelper(node->right);
        cout << node->data << " ";
    }
}

NodePtr RedBlackTree::searchTreeHelper(NodePtr node, int key) {
    if (node == TNULL || key == node->data) {
        return node;
    }

    if (key < node->data) {
        return searchTreeHelper(node->left, key);
    }
    return searchTreeHelper(node->right, key);
}

void RedBlackTree::deleteFix(NodePtr x) {
    NodePtr s;
    while (x != root && x->color == 0) {
        if (x == x->parent->left) {
            s = x->parent->right;
            if (s->color == 1) {
                s->color = 0;
                x->parent->color = 1;
                leftRotate(x->parent);
                s = x->parent->right;
            }

            if (s->left->color == 0 && s->right->color == 0) {
                s->color = 1;
                x = x->parent;
            } else {
                if (s->right->color == 0) {
                    s->left->color = 0;
                    s->color = 1;
                    rightRotate(s);
                    s = x->parent->right;
                }

                s->color = x->parent->color;
                x->parent->color = 0;
                s->right->color = 0;
                leftRotate(x->parent);
                x = root;
            }
        } else {
            s = x->parent->left;
            if (s->color == 1) {
                s->color = 0;
                x->parent->color = 1;
                rightRotate(x->parent);
                s = x->parent->left;
            }

            if (s->right->color == 0 && s->right->color == 0) {
                s->color = 1;
                x = x->parent;
            } else {
                if (s->left->color == 0) {
                    s->right->color = 0;
                    s->color = 1;
                    leftRotate(s);
                    s = x->parent->left;
                }

                s->color = x->parent->color;
                x->parent->color = 0;
                s->left->color = 0;
                rightRotate(x->parent);
                x = root;
            }
        }
    }
    x->color = 0;
}

void RedBlackTree::rbTransplant(NodePtr u, NodePtr v) {
    if (u->parent == nullptr) {
        root = v;
    } else if (u == u->parent->left) {
        u->parent->left = v;
    } else {
        u->parent->right = v;
    }
    v->parent = u->parent;
}

void RedBlackTree::deleteNodeHelper(NodePtr node, int key) {
    NodePtr z = TNULL;
    NodePtr x, y;
    while (node != TNULL) {
        if (node->data == key) {
            z = node;
        }

        if (node->data <= key) {
            node = node->right;
        } else {
            node = node->left;
        }
    }

    if (z == TNULL) {
        cout << "Key not found in the tree" << endl;
        return;
    }

    y = z;
    int y_original_color = y->color;
    if (z->left == TNULL) {
        x = z->right;
        rbTransplant(z, z->right);
    } else if (z->right == TNULL) {
        x = z->left;
        rbTransplant(z, z->left);
    } else {
        y = minimum(z->right);
        y_original_color = y->color;
        x = y->right;
        if (y->parent == z) {
            x->parent = y;
        } else {
            rbTransplant(y, y->right);
            y->right = z->right;
            y->right->parent = y;
        }

        rbTransplant(z, y);
        y->left = z->left;
        y->left->parent = y;
        y->color = z->color;
    }
    delete z;
    if (y_original_color == 0) {
        deleteFix(x);
    }
}

void RedBlackTree::insertFix(NodePtr k) {
    NodePtr u;
    while (k->parent->color == 1) {
        if (k->parent == k->parent->parent->right) {
            u = k->parent->parent->left;
            if (u->color == 1) {
                u->color = 0;
                k->parent->color = 0;
                k->parent->parent->color = 1;
                k = k->parent->parent;
            } else {
                if (k == k->parent->left) {
                    k = k->parent;
                    rightRotate(k);
                }
                k->parent->color = 0;
                k->parent->parent->color = 1;
                leftRotate(k->parent->parent);
            }
        } else {
            u = k->parent->parent->right;

            if (u->color == 1) {
                u->color = 0;
                k->parent->color = 0;
                k->parent->parent->color = 1;
                k = k->parent->parent;
            } else {
                if (k == k->parent->right) {
                    k = k->parent;
                    leftRotate(k);
                }
                k->parent->color = 0;
                k->parent->parent->color = 1;
                rightRotate(k->parent->parent);
            }
        }
        if (k == root) {
            break;
        }
    }
    root->color = 0;
}

void RedBlackTree::printHelper(NodePtr root, bool last) {
    if (root != TNULL) {
        cout << root->data << " ";
        printHelper(root->left,  false);
        printHelper(root->right,  true);
    }
}

RedBlackTree::RedBlackTree() {
    TNULL = new Node;
    TNULL->color = 0;
    TNULL->left = nullptr;
    TNULL->right = nullptr;
    root = TNULL;
}

void RedBlackTree::preorder() {
    preOrderHelper(this->root);
}

void RedBlackTree::inorder() {
    inOrderHelper(this->root);
}

void RedBlackTree::postorder() {
    postOrderHelper(this->root);
}

NodePtr RedBlackTree::searchTree(int k) {
    return searchTreeHelper(this->root, k);
}

NodePtr RedBlackTree::minimum(NodePtr node) {
    while (node->left != TNULL) {
        node = node->left;
    }
    return node;
}

NodePtr RedBlackTree::maximum(NodePtr node) {
    while (node->right != TNULL) {
        node = node->right;
    }
    return node;
}

NodePtr RedBlackTree::successor(NodePtr x) {
    if (x->right != TNULL) {
        return minimum(x->right);
    }

    NodePtr y = x->parent;
    while (y != TNULL && x == y->right) {
        x = y;
        y = y->parent;
    }
    return y;
}

NodePtr RedBlackTree::predecessor(NodePtr x) {
    if (x->left != TNULL) {
        return maximum(x->left);
    }

    NodePtr y = x->parent;
    while (y != TNULL && x == y->left) {
        x = y;
        y = y->parent;
    }

    return y;
}

void RedBlackTree::leftRotate(NodePtr x) {
    NodePtr y = x->right;
    x->right = y->left;
    if (y->left != TNULL) {
        y->left->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nullptr) {
        this->root = y;
    } else if (x == x->parent->left) {
        x->parent->left = y;
    } else {
        x->parent->right = y;
    }
    y->left = x;
    x->parent = y;
}

void RedBlackTree::rightRotate(NodePtr x) {
    NodePtr y = x->left;
    x->left = y->right;
    if (y->right != TNULL) {
        y->right->parent = x;
    }
    y->parent = x->parent;
    if (x->parent == nullptr) {
        this->root = y;
    } else if (x == x->parent->right) {
        x->parent->right = y;
    } else {
        x->parent->left = y;
    }
    y->right = x;
    x->parent = y;
}

void RedBlackTree::insert(int key) {
    NodePtr node = new Node;
    node->parent = nullptr;
    node->data = key;
    node->left = TNULL;
    node->right = TNULL;
    node->color = 1;

    NodePtr y = nullptr;
    NodePtr x = this->root;

    while (x != TNULL) {
        y = x;
        if (node->data < x->data) {
            x = x->left;
        } else {
            x = x->right;
        }
    }

    node->parent = y;
    if (y == nullptr) {
        root = node;
    } else if (node->data < y->data) {
        y->left = node;
    } else {
        y->right = node;
    }

    if (node->parent == nullptr) {
        node->color = 0;
        return;
    }

    if (node->parent->parent == nullptr) {
        return;
    }

    insertFix(node);
}

NodePtr RedBlackTree::getRoot() {
    return this->root;
}

void RedBlackTree::deleteNode(int data) {
    deleteNodeHelper(this->root, data);
}

void RedBlackTree::printTree() {
    if (root) {
        printHelper(this->root, true);
    }
}

void AVL::makeEmpty(AVL_node* t){
    if(t == NULL)
        return;
    makeEmpty(t->left);
    makeEmpty(t->right);
    delete t;
}

AVL_node* AVL::insert(int x, AVL_node* t){
    if(t == NULL){
        t = new AVL_node;
        t->data = x;
        t->height = 0;
        t->left = t->right = NULL;
    }
    else if(x < t->data){
        t->left = insert(x, t->left);
        if(height(t->left) - height(t->right) == 2){
            if(x < t->left->data)
                t = singleRightRotate(t);
            else
                t = doubleRightRotate(t);
        }
    }
    else if(x > t->data){
        t->right = insert(x, t->right);
        if(height(t->right) - height(t->left) == 2){
            if(x > t->right->data)
                t = singleLeftRotate(t);
            else
                t = doubleLeftRotate(t);
        }
    }

    t->height = max(height(t->left), height(t->right))+1;
    return t;
}

AVL_node* AVL::singleRightRotate(AVL_node* &t){
    AVL_node* u = t->left;
    t->left = u->right;
    u->right = t;
    t->height = max(height(t->left), height(t->right))+1;
    u->height = max(height(u->left), t->height)+1;
    return u;
}

AVL_node* AVL::singleLeftRotate(AVL_node* &t){
    AVL_node* u = t->right;
    t->right = u->left;
    u->left = t;
    t->height = max(height(t->left), height(t->right))+1;
    u->height = max(height(t->right), t->height)+1 ;
    return u;
}

AVL_node* AVL::doubleLeftRotate(AVL_node* &t){
    t->right = singleRightRotate(t->right);
    return singleLeftRotate(t);
}

AVL_node* AVL::doubleRightRotate(AVL_node* &t){
    t->left = singleLeftRotate(t->left);
    return singleRightRotate(t);
}

AVL_node* AVL::findMin(AVL_node* t){
    if(t == NULL)
        return NULL;
    else if(t->left == NULL)
        return t;
    else
        return findMin(t->left);
}

AVL_node* AVL::findMax(AVL_node* t){
    if(t == NULL)
        return NULL;
    else if(t->right == NULL)
        return t;
    else
        return findMax(t->right);
}

AVL_node* AVL::remove(int x, AVL_node* t){
    AVL_node* temp;

    if(t == NULL)
        return NULL;

    else if(x < t->data)
        t->left = remove(x, t->left);
    else if(x > t->data)
        t->right = remove(x, t->right);
    else if(t->left && t->right){
        temp = findMin(t->right);
        t->data = temp->data;
        t->right = remove(t->data, t->right);
    }
    else{
        temp = t;
        if(t->left == NULL)
            t = t->right;
        else if(t->right == NULL)
            t = t->left;
        delete temp;
    }
    if(t == NULL)
        return t;

    t->height = max(height(t->left), height(t->right))+1;

    if(height(t->left) - height(t->right) == 2){
        if(height(t->left->left) - height(t->left->right) == 1)
            return singleLeftRotate(t);
        else
            return doubleLeftRotate(t);
    }
    else if(height(t->right) - height(t->left) == 2){
        if(height(t->right->right) - height(t->right->left) == 1)
            return singleRightRotate(t);
        else
            return doubleRightRotate(t);
    }
    return t;
}

int AVL::height(AVL_node* t){
    if(t == NULL){
        return -1;
    }
    return t->height;
}

int AVL::getBalance(AVL_node* t){
    if(t == NULL)
        return 0;
    else
        return height(t->left) - height(t->right);
}

void AVL::inorder(AVL_node* t){
    if(t == NULL)
        return;
    inorder(t->left);
    cout << t->data << " ";
    inorder(t->right);
}

AVL::AVL(){
    root = NULL;
}

void AVL::insert(int x){
    root = insert(x, root);
}

void AVL::remove(int x){
    root = remove(x, root);
}

void AVL::display(){
    inorder(root);
    cout << endl;
}