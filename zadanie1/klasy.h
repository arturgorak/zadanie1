#include <iostream>

using namespace std;

class Sort{
    int size;
    int *array;

public:
    Sort(int array[], int size);
    void print(); // print an array
    void sort(); // sort an array
    void test(); // check that the array is sorted
    void merge_sort(int start, int end);
    void merge(int start, int mid, int end);
    void sort2();
    void heapSort();
};

class Heap{
    int size;
    int fullSize;
    int *array;

public:
    Heap(int array[], int size);
    void Heapify(int heap_size, int i);
    void parent(int index);
    void buildHeap();
    int ExtraMax();
    int returnSize();
    void insert(int x);
    int LeftChild(int x);
    int RightChild(int x);
};

class BST{
public:
    int value;
    BST* left;
    BST* right;
    BST(int value);
    BST& insert(int value);
    bool search(int value);
    BST& remove(int value, BST* parentNode = NULL);
    int getMinValue();
};

struct Node {
    int data;
    Node *parent;
    Node *left;
    Node *right;
    int color;
};

typedef Node *NodePtr;

class RedBlackTree{
private:
    NodePtr root;
    NodePtr TNULL;

    void initializeNULLNode(NodePtr node, NodePtr parent);
    void preOrderHelper(NodePtr node);
    void inOrderHelper(NodePtr node);
    void postOrderHelper(NodePtr node);
    NodePtr searchTreeHelper(NodePtr node, int key);
    void deleteFix(NodePtr x);
    void rbTransplant(NodePtr u, NodePtr v);
    void deleteNodeHelper(NodePtr node, int key);
    void insertFix(NodePtr k);
    void printHelper(NodePtr root, bool last);

public:
    RedBlackTree();
    void preorder();
    void inorder();
    void postorder();
    NodePtr searchTree(int k);
    NodePtr minimum(NodePtr node);
    NodePtr maximum(NodePtr node);
    NodePtr successor(NodePtr x);
    NodePtr predecessor(NodePtr x);
    void leftRotate(NodePtr x);
    void rightRotate(NodePtr x);
    void insert(int key);
    NodePtr getRoot();
    void deleteNode(int data);
    void printTree();
};

struct AVL_node{
    int data;
    AVL_node* left;
    AVL_node* right;
    int height;
};

class AVL{
    AVL_node* root;
    void makeEmpty(AVL_node* t);
    AVL_node* insert(int x, AVL_node* t);
    AVL_node* singleRightRotate(AVL_node* &t);
    AVL_node* singleLeftRotate(AVL_node* &t);
    AVL_node* doubleLeftRotate(AVL_node* &t);
    AVL_node* doubleRightRotate(AVL_node* &t);
    AVL_node* findMin(AVL_node* t);
    AVL_node* findMax(AVL_node* t);
    AVL_node* remove(int x, AVL_node* t);
    int height(AVL_node* t);
    int getBalance(AVL_node* t);
    void inorder(AVL_node* t);
public:
    AVL();
    void insert(int x);
    void remove(int x);
    void display();
};


